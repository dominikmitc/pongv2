let width = 640;
let height = 480;
let lastFrame;
let beep1 = new Audio('beep1.wav');

let video = document.getElementById('myCam');

let videoCanvas = document.getElementById('videocanvas');
videoCanvas.width = width;
videoCanvas.height = height;
let vcontext = videoCanvas.getContext('2d');

let motionCanvas = document.getElementById('motionpreview');
motionCanvas.width = width;
motionCanvas.height = height;
let mcontext = motionCanvas.getContext('2d');

let config = {
    audio: false,
    video: {width: width, height: height}
}

navigator.mediaDevices.getUserMedia(config).then(success).catch(error)

function success(videostream){
    video.srcObject = videostream
}

function error(err){
    console.log(err)
}

// kopiert den mediastream vom videoelement zum canvaselement
function copyStream() {
    vcontext.scale(-1,1);
    vcontext.drawImage(video, 0,0, -width, height )
}

function createMotionpreview(){
    
    let currentFrame =vcontext.getImageData(0, 0, width, height)
    if (!lastFrame) lastFrame = currentFrame;
    
    let calculatedFrame = mcontext.createImageData(width, height)
  
    for (let i = 0; i <  currentFrame.data.length; i+=4 ){
        let averageCurrent = (currentFrame.data[i] + currentFrame.data[i+1] + currentFrame.data[i+2]) / 3
        let averageOld = (lastFrame.data[i] + lastFrame.data[i+1] + lastFrame.data[i+2]) / 3
        let diff = averageCurrent - averageOld;
        
        let maximum = 0;
    
        if (diff > 15) { maximum = 255 }
        calculatedFrame.data[i] = maximum
        calculatedFrame.data[i+1] =  0
        calculatedFrame.data[i+2] =  0   
        calculatedFrame.data[i+3] =  255  
    }
    mcontext.putImageData(calculatedFrame, 0 ,0)
    lastFrame = currentFrame;
}


let left = false;
let right = false;

function checkhotspot(){
    
    let hotspot = mcontext.getImageData(0,0,100,100)
    let summe = 0;
    for (let i = 0; i < hotspot.data.length; i += 1) {     summe += hotspot.data[i]    }
    let durchschnitt = summe / hotspot.data.length
    
    let hotspot1 = mcontext.getImageData(540,0,100,100)
    let summe1 = 0;
    for (let i = 0; i < hotspot1.data.length; i += 1) {     summe1 += hotspot1.data[i]    }
    let durchschnitt1 = summe1 / hotspot1.data.length
    
    mcontext.strokeStyle = "#FF0000";
   
    mcontext.rect(0, 0, 100, 100);
    mcontext.stroke();
    mcontext.rect(540, 0, 100, 100);
    mcontext.stroke();
    
 
    if (durchschnitt1 > 67) {   left= false; right = true; console.log("right") }
    else if (durchschnitt > 67) {  left = true; right = false;   console.log("left") }
    else { left= false; right = false; }

    
}

function capture(){
    copyStream();
    createMotionpreview();
    checkhotspot()
}

setInterval(capture, 17   );






















